package kh.edu.rupp.fe.visitme.view.activity

import android.content.Context
import android.content.Intent
import com.squareup.picasso.Picasso
import kh.edu.rupp.fe.visitme.databinding.ActivityProvinceDetailBinding
import kh.edu.rupp.fe.visitme.model.api.model.Province

class ProvinceDetailActivity: BaseActivity<ActivityProvinceDetailBinding>(ActivityProvinceDetailBinding::inflate) {

    override fun bindUi() {

        val province = intent.getSerializableExtra(EXTRA_PROVINCE) as Province
        Picasso.get().load(province.imageUrl).into(binding.imgProvince)
        binding.txtTitle.text = province.name

    }

    override fun setupListeners() {

    }

    override fun setupObservers() {

    }

    override fun initActions() {

    }

    companion object {

        private const val EXTRA_PROVINCE = "province"

        fun newIntent(context: Context, province: Province): Intent {

            val intent = Intent(context, ProvinceDetailActivity::class.java)
            intent.putExtra(EXTRA_PROVINCE, province)
            return intent

        }

    }

}