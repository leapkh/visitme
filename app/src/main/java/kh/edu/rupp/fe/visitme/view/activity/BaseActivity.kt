package kh.edu.rupp.fe.visitme.view.activity

import android.app.ProgressDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.viewbinding.ViewBinding

abstract class BaseActivity<T : ViewBinding>(private val bindingFunction: (LayoutInflater) -> T) :
    AppCompatActivity() {

    private var loadingDialog: ProgressDialog? = null

    private var _binding: T? = null
    protected val binding: T
        get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        _binding = bindingFunction(layoutInflater)
        setContentView(binding.root)

        bindUi()
        setupListeners()
        setupObservers()
        initActions()
    }

    abstract fun bindUi()

    abstract fun setupListeners()

    abstract fun setupObservers()

    abstract fun initActions()

    fun showLoading() {
        if (loadingDialog == null) {
            loadingDialog = ProgressDialog(this)
        }

        loadingDialog!!.show()
    }

    fun hideLoading() {
        loadingDialog?.hide()
    }

    fun showDialog() {

    }

    fun showLongToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    fun showShortToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

}