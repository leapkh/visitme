package kh.edu.rupp.fe.visitme.view.activity

import androidx.fragment.app.Fragment
import kh.edu.rupp.fe.visitme.R
import kh.edu.rupp.fe.visitme.databinding.ActivityMainBinding
import kh.edu.rupp.fe.visitme.view.fragment.*

class MainActivity2 : BaseActivity<ActivityMainBinding>(ActivityMainBinding::inflate) {

    override fun bindUi() {
        showFragment(HomeFragment())
    }

    override fun setupListeners() {
        binding.bottomNavigationView.setOnItemSelectedListener {

            when (it.itemId) {
                R.id.mnuHome -> showFragment(HomeFragment())
                R.id.mnuProvinces -> showFragment(Provinces2Fragment())
                R.id.mnuSearch -> showFragment(SearchFragment())
                R.id.mnuProfile -> showFragment(ProfileFragment())
                else -> showFragment(MoreFragment())
            }

            true
        }
    }

    override fun setupObservers() {

    }

    override fun initActions() {

    }

    private fun showFragment(fragment: Fragment) {

        // FragmentTransaction
        val fragmentTransition = supportFragmentManager.beginTransaction()

        // Replace fragment in lytFragment
        fragmentTransition.replace(R.id.lytFragment, fragment)

        // Commit transaction
        fragmentTransition.commit()

    }

}