package kh.edu.rupp.fe.visitme.core

import android.app.Application
import kh.edu.rupp.fe.visitme.model.api.model.Province

class AppCore : Application() {

    var provinces: List<Province>? = null

    override fun onCreate() {
        super.onCreate()

        instance = this
    }

    companion object {

        private var instance: AppCore? = null

        fun get(): AppCore {
            return instance!!
        }

    }

}